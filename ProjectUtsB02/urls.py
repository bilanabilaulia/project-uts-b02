"""ProjectUtsB02 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django import urls
from django.urls.conf import include
import saran.urls as saran
import daftar.urls as daftar
import feedback.urls as feedback
import user_profile.urls as profile
from login.views import logoutUser
from daftar.views import registFlutter, index
import login.urls as login_url
import dana.urls as dana
import pengutaraan.urls as pengutaraan
import profil.urls as profil
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('saran/', include(saran)),
    path('login/', include(login_url)),
    path('feedback/', include(feedback)),
    path('daftar/', index, name="daftar"),
    path('profile/', include(profile)),
    path('logout/', logoutUser, name='logoutUser'),
    path('dana/', include(dana)),
    path('pengajuan/', include(pengutaraan)),
    path('profil/', include(profil)),
    path('daftar/flutterregist', registFlutter, name="flutter-regist"),
]
