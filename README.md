<!-- HelPINK U -->

# Project UTS B02
Link Heroku: `https://helpinku.herokuapp.com/`
Link Gitlab: `https://gitlab.com/bilanabilaulia/project-uts-b02`

## Anggota kelompok B02

1. Atifah Nabilla Al Qibtiyah - 2006462802
2. Febrian Rayhan Aryadianto - 2006482520
3. Irham Wahyu Arieffadhila - 2006485863
4. Nabila Aulia Rahardja - 2006482243
5. Muhammad Syahrul Adji - 2006463805
6. Omarenzo Zafira Thahir - 2006484785
7. Nafidz Abiyyu Hanief - 2006485850

## Tema 
Aplikasi pendataan mahasiswa terkena covid 

## Aplikasi dan Manfaat
**HelPINK U** mewadahi mahasiswa yang terkena COVID-19 untuk meminta dan menerima bantuan penanganan lebih lanjut. Selain itu, terdapat opsi bagi mereka yang ingin memberi bantuan berupa dana yang nantinya akan kami salurkan baik dengan uang maupun obat-obatan. Website ini juga menerima permintaan bantuan tempat untuk isolasi mandiri dan permintaan bantuan penanganan dari RSUI. Nantinya, pengumpulan dana akan dilakukan oleh pemberi bantuan dan tindak lanjut akan permintaan bantuan dilakukan oleh fakultas selaku admin.

## Opsi bantuan 
- Dana bantuan
- Tempat isolasi mandiri di asrama UI
- Bantuan penanganan medis dari RSUI

## Modul
- **Masuk** = membuat halaman login dan landing page untuk masing-masing jenis pengguna.
- **Profil (edit)** = membuat profil untuk masing-masing pengguna dan mengimplementasikan fitur edit profil.
- **Daftar** = membuat form akun login untuk masing-masing jenis pengguna.
- **Saran** = membuat form saran dari Mahasiswa Fasilkom dan Warga Fasilkom ke admin yang nantinya akan dimunculkan dalam bentuk card pada page admin.
- **Pengajuan isolasi, pengajuan dana & bantuan penanganan medis** = membuat form dari Mahasiswa Fasilkom yang berisi latar belakang dari pengajuan yang dilakukan oleh Mahasiswa Fasilkom.
- **Dana** = membuat form pengisian dana, daftar pilihan rekening, dan menampilkan data-data tersebut di page admin.
- **Mengirim feedback status oleh admin** = membuat sistem pengiriman feedback status oleh admin berdasarkan pengajuan yang dilakukan pengaju bahwa telah disetujui ataupun ditolak.

## Rincian Modul
- **Memberi bantuan** = modul ini digunakan untuk proses pemberian bantuan dari warga Fasilkom UI kepada mahasiswa Fasilkom UI yang mempunyai alur sebagai berikut.
   1. Warga Fasilkom UI diarahkan untuk login sebagai pemberi bantuan dengan menggunakan email pribadi dan password
   2. Mengisi data diri dan menyetujui semua persyaratan yang tercantum
   3. User akan diarahkan ke halaman pemberian bantuan dana.
   4. User mengisi form nominal dan melakukan transaksi.
   5. Jika sudah, maka user akan mengklik tombol verifikasi.
- **Melapor telah terkena covid-19** = modul ini digunakan sebagai sarana untuk pasien COVID yang membutuhkan bantuan agar nantinya dapat didata untuk disalurkan bantuannya. Berikut adalah alurnya.
   1. Mahasiswa terlebih dahulu  mendaftar sebagai penerima bantuan dengan menyertakan email pribadi dan password. Jika sudah punya akun, user harus login terlebih dahulu.
   2. Jika user mendaftar maka harus mengisi data diri dan menyetujui persyaratan yang berlaku.
   3. User akan diarahkan ke halaman penerima bantuan.
   4. Memilih bantuan apa yang diperlukan
- **Memberi saran kepada HelPINK U** = modul ini digunakan untuk memberikan saran kepada pihak pengelola **HelPINK U** mengenai web application yang dibuat dan dikelola oleh **HelPINK U**, baik mengenai kemudahan menggunakan fitur maupun kritik terhadap fitur yang sudah ada. Berikut alurnya.
   1. Mahasiswa Fasilkom UI dan Warga Fasilkom UI melakukan login akun terlebih dahulu baik sebagai penerima bantuan bagi akun Mahasiswa Fasilkom UI maupun sebagai pemberi bantuan bagi akun Warga Fasilkom UI
   2. Jika user belum memiliki akun, maka user terlebih dahulu diminta untuk mendaftarkan akun beserta jenis akunnya
   3. Jika user sudah memiliki akun, maka user dapat memilih opsi pada navigation bar untuk memasuki page Contact Us
   4. Pada page Contact Us, user dapat menuliskan saran atau kritik mereka kepada pihak pengelola **HelPINK U**.

## Kerangka
1. Landing page (Jumlah data mahasiswa yang membutuhkan bantuan, jumlah bantuan yang masuk, dokumentasi )
2. Halaman masuk 
3. Halaman daftar
4. Page permintaan bantuan bagi penerima bantuan (Bantuan apa yg diperlukan)
5. Halaman donasi bagi donatur (Nomor rekening, dana akan disalurkan kemana saja)
6. Halaman khusus admin (Semua data penerima bantuan, saran dan kritik)
7. Fitur Contact Us (Inbox untuk keluhan dan saran, beserta pencatuman call center)

## Persona
1. **Mahasiswa Fasilkom UI** = mempunyai peran untuk melapor atau memberi bantuan pada web. Jika melapor, asumsikan mahasiswa Fasilkom UI tersebut sedang terkena covid-19, maka mengisi data diri serta bukti bahwa sedang terkena covid-19. Sedangkan, jika memberi bantuan, mahasiswa Fasilkom UI tersebut dapat memilih bantuan apa yang ingin diberikan kepada mahasiswa Fasilkom UI yang terkena covid-19. Fitur “Contact Us” dapat diakses oleh Mahasiswa Fasilkom UI.
   - Sebelum login = Hanya dapat mengakses landing page yang berisi informasi umum yang dapat diakses oleh masyarakat umum juga, yaitu data mahasiswa yang terkena COVID, total dana bantuan yang masuk, dan tombol login.
   - Setelah login = Mahasiswa dapat mengakses fitur untuk mengajukan bentuk bantuan. 
2. **Warga Fasilkom UI**= mempunyai peran sebagai pemberi bantuan mahasiswa Fasilkom UI yang sedang terkena covid-19 dengan alur, login dengan akun e-mail sebagai pemberi bantuan, lalu isi data diri dan menyetujui semua persyaratan yang tercantum, kemudian pilih bantuan apa yang ingin diberikan dan ikuti semua proses lanjutannya hingga selesai. Fitur “Contact Us” dapat diakses Warga Fasilkom UI.
   - Sebelum login = Hanya dapat mengakses landing page yang berisi informasi umum yang dapat diakses oleh masyarakat umum juga, yaitu data mahasiswa yang terkena COVID, total dana bantuan yang masuk, dan tombol login
   - Setelah login = Warga Fasilkom yang telah login sebagai donatur dapat mengisi form yang berisi nominal uang yang akan disumbangkan dan melakukan verifikasi apakah pengguna telah mentransfer dana.
3. **Kemahasiswaan Fasilkom UI** = mempunyai peran sebagai pengamat dalam jalannya proses dan keamanan data diri Mahasiswa Fasilkom UI yang digunakan dalam proses pada web berlangsung. Kemahasiswaan akan login sebagai admin.
   - Sebelum login = Hanya dapat mengakses landing page yang berisi informasi umum yang dapat diakses oleh masyarakat umum juga, yaitu data mahasiswa yang terkena COVID, total dana bantuan yang masuk, dan tombol login
   - Setelah login = Kemahasiswaan Fasilkom dapat melihat data yang telah dimasukkan oleh Mahasiswa Fasilkom UI yang membutuhkan bantuan serta detail bantuan yang diperlukan, seperti membutuhkan tempat untuk isolasi mandiri (khusus mahasiswa yang berdomisili di Jabodetabek), penanganan medis pada RSUI (khusus mahasiswa yang berdomisili di Jabodetabek), serta dana untuk keperluan selama isolasi mandiri di rumah masing-masing.
4. **Admin** = mempunyai peran sebagai pengontrol web **helPINK U** dalam menjaga proses pengolahan dan keamanan data tersebut berjalan dengan lancar. Admin memiliki tanggung jawab untuk mengatur semua modul yang ada pada web **helPINK U**.
   - Sebelum login = Hanya dapat mengakses landing page yang berisi informasi umum yang dapat diakses oleh masyarakat umum juga, yaitu data mahasiswa yang terkena COVID, total dana bantuan yang masuk, dan tombol login.
   - Setelah login = Admin mendapatkan akses ke seluruh data yang terdapat pada web dan bertanggung jawab baik atas tampilan data maupun sumber data yang ada.
