from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from main.views import home_admin, home_donatur, home_pengaju
# Create your views here.

def login_ajax(request):
    if request.method == 'POST' and request.is_ajax:
        print(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            group = None
            if user.groups.all()[0].name == 'pemberi':
                login(request, user=user)
                group = 'pemberi'
            elif user.groups.all()[0].name == 'penerima':
                login(request, user=user)
                group = 'penerima'
            elif user.groups.all()[0].name == 'admin':
                login(request, user=user)
                group = 'admin'
        
            return JsonResponse({'group':group,'status':'berhasil'})
        else:
            return JsonResponse({'status':'gagal'})
    context = {}
    return render(request, 'login.html', context)

def logoutUser(request):
    logout(request)
    return HttpResponseRedirect('/')

@csrf_exempt
def flutter_login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        if user.is_active:
            group = None
            if user.groups.all()[0].name == 'pemberi':
                login(request, user=user)
                group = 'pemberi'
            elif user.groups.all()[0].name == 'penerima':
                login(request, user=user)
                group = 'penerima'
            elif user.groups.all()[0].name == 'admin':
                login(request, user=user)
                group = 'admin'
        
            return JsonResponse({'group':group,'status':True}, status=200)
        else:
            return JsonResponse({'status': False,
                                'message': 'Failed!'},
                                status=401)
    else:
        return JsonResponse({'status': False,
                            'message': 'Username/Password salah!'},
                            status=401)


@csrf_exempt
def flutter_logout(request):
    try:
        logout(request)
        return JsonResponse({
            "status": True,
            "message": "Successfully Logged out!"
        }, status=200)
    except:
        return JsonResponse({
            "status": False,
            "message": "Failed to Logout"
        }, status=401)
