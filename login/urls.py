from django.urls import path
from .views import flutter_login, flutter_logout, logoutUser, login_ajax


urlpatterns = [
    path('', login_ajax, name='login'),
    path('logout/', logoutUser,name='logoutUser'),
    path('loginFlutter', flutter_login, name="LoginFlutter"),
    path('logoutFlutter', flutter_logout, name="LogoutFlutter"),
]