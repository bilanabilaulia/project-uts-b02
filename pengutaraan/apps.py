from django.apps import AppConfig


class PengutaraanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pengutaraan'
