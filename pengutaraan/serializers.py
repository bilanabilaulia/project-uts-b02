from rest_framework import serializers
from feedback.models import DataAjuan

class PengajuanSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'nama',
            'tipePengajuan',
            'latar',
            'lokasi',
            'status'
        )
        model=DataAjuan