from django.shortcuts import render, redirect
from feedback.models import DataAjuan
from feedback.forms import AjuanForm
from login.decorators import allowed_users
from django.contrib.auth.decorators import login_required
from django.core import serializers
from rest_framework import generics
from django.http.response import HttpResponse
from.serializers import PengajuanSerializer

# Create your views here.
class listPengajuan(generics.ListCreateAPIView):
    queryset= DataAjuan.objects.all()
    serializer_class = PengajuanSerializer

class detailPengajuan(generics.RetrieveUpdateDestroyAPIView):
    queryset= DataAjuan.objects.all()
    serializer_class = PengajuanSerializer

@login_required(login_url='login')
@allowed_users(allowed_roles=['penerima','admin'])
def index(request):
    dataajuan = DataAjuan.objects.all()
    context = {'dataajuan':dataajuan}
    return render(request,'pengajuan.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['penerima','admin'])
def form_pengajuan(request):
    form = AjuanForm()
    if request.method == 'POST':
        form = AjuanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/pengajuan')

    context = {'form':form}
    return render(request, 'form_pengajuan.html', context)
from django.shortcuts import render

def json (request):
    data = serializers.serialize('json', DataAjuan.objects.all())
    return HttpResponse(data, content_type="application/json")