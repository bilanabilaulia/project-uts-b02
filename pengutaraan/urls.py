from django.urls import path
from .views import index, form_pengajuan, json, listPengajuan, detailPengajuan

urlpatterns = [
    path('', index, name='pengajuan'),
    path('form_pengajuan', form_pengajuan, name='form_pengajuan'),
    path('pengajuan_json', json, name='json'),
    path('json/', listPengajuan.as_view()),
    path('<int:pk>/', detailPengajuan.as_view())
]