from django.contrib.auth.models import User
from django.db import models
from django.db.models.query import QuerySet
from django.http import response
from django.shortcuts import redirect, render, get_object_or_404
from .models import Profil
from .forms import ProfilForm
from django.urls import reverse
from login.decorators import allowed_users
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.core import serializers
from .serializers import ProfilSerializers
from rest_framework import generics
# Create your views here.
class listProfil(generics.ListCreateAPIView):
    queryset = Profil.objects.all()
    serializer_class = ProfilSerializers

class detailProfil(generics.RetrieveUpdateAPIView):
    queryset = Profil.objects.all()
    serializer_class = ProfilSerializers

@login_required(login_url='login')
def create_profil(request):
    form = ProfilForm()
    if request.method == 'POST':
        form = ProfilForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/profil')
            
    context = {'form' : form}
    return render(request, 'form_profil.html', context)

@login_required(login_url='login')
def edit_profil(request, pk):
    #profil = Profil.objects.filter(user__username=request.user).first()
    key = Profil.objects.get(id=pk)
    #key = get_object_or_404(User, id=pk)
    form = ProfilForm(instance=key)
    
    if request.method == 'POST':
        form = ProfilForm(request.POST, instance=key)
        if form.is_valid():
            form.save()
            return redirect('/profil')
            
    context = {'form' : form}
    return render(request, 'form_profil.html', context)
    

@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin'])
def profil_view(request):
    #profil = Profil.objects.filter(user__username=request.user)
    profil = Profil.objects.filter(user__username=request.user)
    context = {'profil':profil}
    return render(request, 'profil.html', context)

def json(request):
    data = serializers.serialize('json', Profil.objects.all())
    return HttpResponse(data, content_type = "application/json")