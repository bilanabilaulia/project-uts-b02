from django import forms
from django.db.models import fields
from .models import Profil

class ProfilForm(forms.ModelForm):
    class Meta:
        model = Profil
        fields = '__all__'
        exclude = ['user']