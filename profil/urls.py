from django.urls import path
from . import views
from django.conf.urls import url
from .views import listProfil, detailProfil

urlpatterns = [
    path('create_profil/', views.create_profil, name = 'create_profil'),
    path('edit_profil/<str:pk>/', views.edit_profil, name='edit_profil'),
    path('', views.profil_view, name='profil_pengguna'),
    path('json', views.json, name="json"),
    path('jsonprofil/', listProfil.as_view()),
    path('<int:pk>/', detailProfil.as_view())

]
