from django.contrib.auth import models
from rest_framework import fields, serializers
from .models import Profil

class ProfilSerializers(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'user',
            'name',
            'email',
            'role',
            'gender',
            'phone',
            'birthday',
            'address'
        )
        model = Profil