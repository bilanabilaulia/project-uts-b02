from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Profil(models.Model):
    gender_choices = [
        (("Laki-laki", "Laki-laki")),
        (("Perempuan", "Perempuan")),
    ]

    role_choices = [
        (("Admin", "Admin")),
        (("Pengaju", "Pengaju")),
        (("Donatur", "Donatur")),
    ]

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=300, null=True)
    email = models.EmailField(max_length=300, null=True)
    role = models.CharField(choices=role_choices, null=True, blank=True, max_length=300)
    gender = models.CharField(choices=gender_choices, null=True, blank=True, max_length=300)
    phone = models.CharField(max_length=300, null=True)
    birthday = models.DateField(default='2002-01-31')
    address = models.CharField(max_length=300, null=True)


    def __str__(self):
        return self.name
    

