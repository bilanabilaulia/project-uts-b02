from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('pengaju/', views.home_pengaju, name='home_pengaju'),
    path('donatur/', views.home_donatur, name='home_donatur'),
    path('adm/', views.home_admin, name='home_admin'),
]
