from django.shortcuts import render
from login.decorators import allowed_users
from django.contrib.auth.decorators import login_required



def home(request):
    return render(request, 'main/home.html')

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def home_admin(request):
    return render(request, 'home_admin.html')

@login_required(login_url='login')
@allowed_users(allowed_roles=['pemberi','admin'])
def home_donatur(request):
    return render(request, 'home_donatur.html')


@login_required(login_url='login')
@allowed_users(allowed_roles=['penerima','admin'])
def home_pengaju(request):
    return render(request, 'home_pengaju.html')

