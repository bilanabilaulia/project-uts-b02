from rest_framework import serializers
from feedback import models

class feedbackSerializers(serializers.ModelSerializer):
    class Meta:
        fields = (
            'nama',
            'tipePengajuan',
            'latar',
            'lokasi',
            'status'
        )

        model = models.DataAjuan