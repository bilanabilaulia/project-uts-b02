from django.db import models

class DataAjuan(models.Model):
    STATUS = (
        ('Diterima', 'Diterima'),
        ('Ditolak', 'Ditolak'),
        ('Pending', 'Pending'),
    )

    TIPE = (
        ('Dana','Dana'),
        ('Tempat Isolasi', 'Tempat Isolasi'),
        ('Bantuan Medis', 'Bantuan Medis'),
    )
    nama = models.CharField(max_length=200, null=True)
    tipePengajuan = models.CharField(max_length=200, null=True, choices=TIPE)
    latar = models.CharField(max_length=200, null=True)
    lokasi = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=200, null=True, choices=STATUS)

    def __str__(self):
        return self.nama