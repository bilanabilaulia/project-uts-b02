from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls import url
from .views import listFeedback, detailFeedback

urlpatterns = [
    path('', views.index, name="feedback"),
    path('status/', views.status, name="status"),
    path('create_pengaju/', views.create_pengaju, name="create_pengaju"),
    path('update_data/<str:pk>/', views.update_data, name="update_data"),
    path('delete_data/<str:pk>/', views.delete_data, name="delete_data"),
    path('', listFeedback.as_view()),
    path('<int:pk>', detailFeedback.as_view())

]

