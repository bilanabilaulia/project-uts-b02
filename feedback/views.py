from django.shortcuts import render, redirect
from .models import DataAjuan
from .forms import AjuanForm
from login.decorators import allowed_users
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from rest_framework import generics

# Create your views here.
from feedback import models
from .serializers import feedbackSerializers

class listFeedback(generics.ListCreateAPIView):
    queryset = models.DataAjuan.objects.all()
    serializer_class = feedbackSerializers

class detailFeedback(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.DataAjuan.objects.all()
    serializer_class = feedbackSerializers

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def index(request):
    dataajuan = DataAjuan.objects.all()

    total_pengajuan = dataajuan.count()
    status_pending = dataajuan.filter(status='Pending').count()
    status_diterima = dataajuan.filter(status='Diterima').count()

    context = {'dataajuan':dataajuan, 'total_pengajuan':total_pengajuan, 'status_pending':status_pending,
               'status_diterima':status_diterima}

    return render(request, 'feedback.html', context)

def status(request):
    return render(request, 'status.html')

def create_pengaju(request):
    form = AjuanForm()
    if request.method == 'POST':
        form = AjuanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/feedback')

    context = {'form':form}
    return render(request, 'form_create.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def update_data(request, pk):
    key = DataAjuan.objects.get(id=pk)
    form = AjuanForm(instance=key)

    if request.method == 'POST':
        form = AjuanForm(request.POST, instance=key)
        if form.is_valid():
            form.save()
            return redirect('/feedback')

    context = {'form': form}
    return render(request, 'form_create.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def delete_data(request, pk):
    key = DataAjuan.objects.get(id=pk)
    if request.method == 'POST':
        key.delete()
        return redirect('/feedback')

    context = {'item':key}
    return render(request, 'delete.html', context)