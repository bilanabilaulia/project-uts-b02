from django.forms import ModelForm
from .models import DataAjuan

class AjuanForm(ModelForm):
    class Meta:
        model = DataAjuan
        fields = "__all__"