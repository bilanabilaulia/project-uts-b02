from django.http import response
from django.http.response import HttpResponseRedirect,JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from main.views import home
from .forms import CreateUser
from .decorators import allowed_users, unauthenticated_user
from django.contrib.auth.models import Group, User
from django.views.decorators.csrf import csrf_exempt
import json as jsons

# Create your views here.
@unauthenticated_user
def index(request):
    context = {}
    form = CreateUser()
    if request.method == 'POST' and request.is_ajax():
        form = CreateUser(request.POST)
        role = request.POST.get('btnradio')
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            
            if role == 'pemberi':
                user.groups.add(Group.objects.get(name='pemberi'))
                user.is_admin = False
                user.save()
                messages.success(request, 'Account was created for ' + username)
                return JsonResponse({'msg': 'Success'})
            else :
                user.groups.add(Group.objects.get(name='penerima'))
                user.is_admin = False
                user.is_admin = False
                user.save()
                messages.success(request, 'Account was created for ' + username)
                return JsonResponse({'msg': 'Success'})

        else:
            messages.error(request, 'Data yang dimasukkan tidak sesuai, harap isi data diri kembali')
            return JsonResponse({'msg': 'Failed' })

    context['form'] = form
    return render (request, 'register.html', context)

@csrf_exempt
def registFlutter(request):

    if (request.method == 'POST'):
        data = jsons.loads(request.body)

        username = data["username"]
        email = data["email"]
        password = data["password"]
        
        newUser = User.objects.create_user(
        username = username, 
        email = email,
        password = password
        )
        

        newUser.save()
        if data['btnradio'] == 'pemberi':
            group = Group.objects.get(name='pemberi')
            newUser.groups.add(group)
        else:
            group = Group.objects.get(name='penerima')
            newUser.groups.add(group)
        return JsonResponse({'status': 200})


