from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext as _

# Create your models here.
class Profile(models.Model):
    GENDER_MALE = 1
    GENDER_FEMALE = 2
    gender_choices = [
        (GENDER_MALE, _("Laki-laki")),
        (GENDER_FEMALE, _("Perempuan")),
    ]

    ROLE_ADMIN = 1
    ROLE_PENGAJU = 2
    ROLE_DONATUR = 3
    role_choices = [
        (ROLE_ADMIN, _("Admin")),
        (ROLE_PENGAJU, _("Pengaju")),
        (ROLE_DONATUR, _("Donatur")),
    ]

    
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    role = models.PositiveSmallIntegerField(choices=role_choices, null=True, blank=True)
    gender = models.PositiveSmallIntegerField(choices=gender_choices, null=True, blank=True)
    phone = models.CharField(max_length=200, null=True)
    birthday = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=200, null=True)

    @receiver(post_save, sender=User)
    def create_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_profile(sender, instance, **kwargs):
        instance.profile.save()   
# Create your models here.
