from django.shortcuts import render, redirect

from .serializers import DonasiSerializer
from .forms import FormDonasi
from .models import Donasi
from rest_framework import generics

# Create your views here.

class listDonasi (generics.ListCreateAPIView):
    queryset= Donasi.objects.all()
    serializer_class = DonasiSerializer

class detailDonasi (generics.RetrieveUpdateDestroyAPIView):
    queryset= Donasi.objects.all()
    serializer_class = DonasiSerializer

def formulir(request):
    form = FormDonasi(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        
        bank = request.POST.get("bank")
        norekening = request.POST.get("norekening")
        jmldonasi = request.POST.get("jmldonasi")
        pilihan = request.POST.get("pilihan")
        return redirect('dana/donasi_berhasil.html')
    else:
       return render(request, 'dana/formulir_donasi.html', {"form": form})

def berhasil(request):
    response={}
    return render(request,'dana/donasi_berhasil.html',response)
