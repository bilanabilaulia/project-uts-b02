from rest_framework import serializers

from.models import Donasi

class DonasiSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'bank',
            'norekening',
            'jmldonasi'
        )
        model=Donasi