from django.contrib import admin
from django.urls import path, include
from . import views 
from .views import listDonasi, detailDonasi

app_name = 'dana'

urlpatterns = [
    path('formulir_donasi/', views.formulir, name='formulir_donasi'),
    path('donasi_berhasil/', views.berhasil, name='donasi_berhasil'),
    path('json/', listDonasi.as_view()),
    path('<int:pk>/', detailDonasi.as_view())
]
