from django.db import models

# Create your models here.
class Donasi(models.Model):
    bank = models.CharField(max_length=200, blank=True)
    norekening = models.CharField(max_length=100, blank= True)
    jmldonasi = models.IntegerField()
