from django.urls import path
#from .views import index, form_saran_admin
from . import views
from.views import listSaran, detailSaran

urlpatterns = [
    path('saran_admin/', views.index, name='index'),
    path('form_saran_admin/', views.form_saran_admin, name='form_saran_admin'),
    path('form_saran_pengaju/', views.form_saran_pengaju, name='form_saran_pengaju'),
    path('form_saran_donatur/', views.form_saran_donatur, name='form_saran_donatur'),
    path('saran_json/', views.json, name='json'),
    path('json/', listSaran.as_view()),
    path('<int:pk>/', detailSaran.as_view())
]