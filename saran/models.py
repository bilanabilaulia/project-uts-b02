from django.db import models

# Create your models here.

Rate_CHOICES=[('1','1'),
            ('2','2'),
            ('3','3'),
            ('4', '4'),
            ('5', '5')]
Role_CHOICES=[('Admin', 'Admin'),
            ('Donatur', 'Donatur'),
            ('Pengaju', 'Pengaju')]

class Saran(models.Model):
    role = models.CharField(max_length=10, choices=Role_CHOICES)
    keluhan = models.CharField(max_length=50)
    pesan = models.CharField(max_length=500)
    rating = models.CharField(max_length=10, choices=Rate_CHOICES, default='5')
