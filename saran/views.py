from django.db import models
from django.http.response import JsonResponse
from django.shortcuts import render
from.models import Saran
from.forms import SaranForm
from login.decorators import allowed_users
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.core import serializers
from.serializers import SaranSerializer
from rest_framework import generics
# Create your views here.

class listSaran (generics.ListCreateAPIView):
    queryset= Saran.objects.all()
    serializer_class = SaranSerializer

class detailSaran (generics.RetrieveUpdateDestroyAPIView):
    queryset= Saran.objects.all()
    serializer_class = SaranSerializer

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def index (request):
    saran = Saran.objects.all().values()
    response = {'saran2': saran}
    return render(request, 'saran_admin.html', response)

def json (request):
    data = serializers.serialize('json', Saran.objects.all())
    return HttpResponse(data, content_type="application/json")



@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def form_saran_admin(request):
    context ={}
    # create object of form
    form= SaranForm()
    
    # check if form data is valid
    # if form.is_valid():
    #     # save the form data to model
    #     form.save()
    # context['form']= form
    if request.is_ajax():
        form = SaranForm(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({
                'msg': 'Success'
            })
    context['form']= form
    return render(request, "saran_user_admin.html", context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'penerima'])
def form_saran_pengaju(request):
    context ={}
    # create object of form
    form= SaranForm()
    
    # check if form data is valid
    # if form.is_valid():
    #     # save the form data to model
    #     form.save()
    # context['form']= form
    if request.is_ajax():
        form = SaranForm(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({
                'msg': 'Success'
            })
    context['form']= form
    return render(request, "saran_user_pengaju.html", context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'pemberi'])
def form_saran_donatur(request):
    context ={}
    # create object of form
    form= SaranForm()
    
    # check if form data is valid
    # if form.is_valid():
    #     # save the form data to model
    #     form.save()
    # context['form']= form
    if request.is_ajax():
        form = SaranForm(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({
                'msg': 'Success'
            })
    context['form']= form
    return render(request, "saran_user_donatur.html", context)