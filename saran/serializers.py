from rest_framework import serializers

from.models import Saran

class SaranSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'role',
            'keluhan',
            'pesan',
            'rating'
        )
        model=Saran